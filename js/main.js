/*password*/

$(".toggle-password,.toggle-password-new,.toggle-password-login").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});

/*filter*/

$(document).ready(function() {
    $('.portfolio-item').isotope(function() {
        itemSelector: '.item'
    });



    $('.portfolio-menu ul li').click(function() {
        $('.portfolio-menu ul li').removeClass('active');
        $(this).addClass('active');


        var selector = $(this).attr('data-filter');
        $('.portfolio-item').isotope({
            filter: selector
        })
        return false;
    });
});

$(document).ready(function() {
    $('.demo').fSelect();
});